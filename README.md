# README

## Dependencies

- Ruby (current version is in `.ruby-version`)
- Redis

TODO: (Node, Yarn)

## Installation

1. Install `foreman` via `gem install foreman` (see https://github.com/ddollar/foreman#installation for details)
1. `bundle install`

TODO: database creation

## Start on development server

1. Copy `.env` file from template with `cp .env.example .env` command and edit your local settings, if necessary
1. `foreman start`
1. Visit http://localhost:5000 in browser

## How to run test suite

1. `bundle exec rspec`

TODO: database creation
