# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ExchangeRateChannel, type: :channel do
  describe '#subscribed' do
    it 'successfully subscribes' do
      subscribe

      expect(subscription).to be_confirmed
      expect(subscription).to have_stream_from(described_class::CHANNEL_NAME)
    end
  end
end
