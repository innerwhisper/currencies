# frozen_string_literal: true

require 'rails_helper'

RSpec.describe UpdateExternalExchangeRateJob, type: :job do
  describe '#perform' do
    include_context 'with stubbed CBR.ru'

    it 'set new value of external exchange rate' do
      external_exchange_rate = class_double(ExternalExchangeRate, 'value=' => nil)

      described_class.new.perform(external_exchange_rate)

      expect(external_exchange_rate).to have_received(:value=).with(CbrAdapter.one_usd_in_rub)
    end
  end
end
