# frozen_string_literal: true

RSpec.describe ExternalExchangeRate do
  describe '.value=' do
    it 'set value to cache_key' do
      described_class.value = 1.22

      expect(REDIS_CONN.get(described_class.cache_key)).to eq('1.22')
      expect(REDIS_CONN.ttl(described_class.cache_key)).to be_within(2).of(described_class::CACHE_EXPIRATION_DURATION)
    end

    it 'rewrites previously set value' do
      described_class.value = 1.23
      described_class.value = 2.56

      expect(REDIS_CONN.get(described_class.cache_key)).to eq('2.56')
    end

    it 'broadcasts value to Action Cable' do
      value = 1.56

      expect { described_class.value = value }.to have_broadcasted_to(ExchangeRateChannel::CHANNEL_NAME).with(value)
    end
  end

  describe '.value' do
    it 'read value by cache_key' do
      described_class.value = 1.25

      expect(described_class.value).to eq('1.25')
    end

    context 'when value is absent' do
      before do
        described_class.value = ''
      end

      include_context 'with stubbed CBR.ru' do
        let(:exchange_rate_in_response) { '12,235' }
      end

      it 'fetch value via UpdateExternalExchangeRateJob#perform' do
        expect(described_class.value).to eq '12.235'
      end
    end

    it ' value by cache_key' do
      described_class.value = 1.23
    end
  end

  describe '.cache_key' do
    it { expect(described_class.cache_key).to be_a String }
    it { expect(described_class.cache_key).not_to be_empty }
  end
end
