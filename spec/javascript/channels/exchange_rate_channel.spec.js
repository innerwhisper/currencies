import { change_usd_to_rub_exchange_rate } from '../../../app/javascript/channels/exchange_rate_channel.js';

test('changes content of specific element', () => {
  document.body.innerHTML =
    '<div id="usd_to_rub_value">68.6745</div>'

  const value = '2.55';

  change_usd_to_rub_exchange_rate(value);

  expect(document.getElementById('usd_to_rub_value').textContent).toEqual(value);
});
