# frozen_string_literal: true

RSpec.describe CbrAdapter do
  describe '.one_usd_in_rub' do
    let(:exchange_rate_in_response_float) { 12.235 }

    include_context 'with stubbed CBR.ru' do
      let(:exchange_rate_in_response) { '12,235' }
    end

    it 'returns current usd_rub exchange rate' do
      expect(described_class.one_usd_in_rub).to eq(exchange_rate_in_response_float)
    end
  end
end
