# frozen_string_literal: true

RSpec.shared_context 'with stubbed CBR.ru' do
  let(:exchange_rate_in_response) { '11,23' }
  let(:xml_response) do
    "<ValCurs Date=\"02.06.2020\" name=\"Foreign Currency Market\">
    <script type=\"text/javascript\" charset=\"utf-8\" id=\"zm-extension\"/>
    <Valute ID=\"R01235\">
    <NumCode>840</NumCode>
    <CharCode>USD</CharCode>
    <Nominal>1</Nominal>
    <Name>Доллар США</Name>
    <Value>#{exchange_rate_in_response}</Value>
    </Valute>
    <Valute ID=\"R01239\">
    <NumCode>978</NumCode>
    <CharCode>EUR</CharCode>
    <Nominal>1</Nominal>
    <Name>Евро</Name>
    <Value>77,6376</Value>
    </Valute>
    </ValCurs>"
  end

  before do
    stub_request(:get, CbrAdapter::SERVICE_URL)
      .with(
        headers: {
          'Accept' => '*/*',
          'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
          'Host' => CbrAdapter::SERVICE_DOMAIN,
          'User-Agent' => 'Ruby'
        }
      )
      .to_return(status: 200, body: xml_response, headers: {})
  end
end
