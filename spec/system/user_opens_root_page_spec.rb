# frozen_string_literal: true

RSpec.describe 'User opens root page', type: :system do
  let(:current_currency_value) { 22.02 }

  before do
    driven_by(:rack_test)
    # driven_by(:selenium_chrome_headless)

    ExternalExchangeRate.value = current_currency_value
  end

  it 'see standard rails root page' do
    visit root_path

    expect(page).to have_content "Текущий курс доллара:\n#{current_currency_value}\nруб."
  end
end
