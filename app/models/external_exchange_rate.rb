# frozen_string_literal: true

# This class is responsible for storing external exchange rate
class ExternalExchangeRate
  CACHE_EXPIRATION_DURATION = 1.hour

  class << self
    def value=(value)
      REDIS_CONN.setex(cache_key, CACHE_EXPIRATION_DURATION, value)

      ActionCable.server.broadcast ExchangeRateChannel::CHANNEL_NAME, value
    end

    def value(value_updater = UpdateExternalExchangeRateJob.new)
      get_value_proc = proc { REDIS_CONN.get(cache_key) }

      key = get_value_proc.call

      return key if key.present?

      value_updater.perform
      get_value_proc.call
    end

    def cache_key
      'usd_rub_external_exchange_rate'
    end
  end
end
