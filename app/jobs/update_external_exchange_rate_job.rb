# frozen_string_literal: true

# This class is responsible for scheduled update of external exchange rate
class UpdateExternalExchangeRateJob < ApplicationJob
  queue_as :default

  def perform(external_exchange_rate = ExternalExchangeRate)
    external_exchange_rate.value = CbrAdapter.one_usd_in_rub
  end
end
