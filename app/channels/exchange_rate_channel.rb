# frozen_string_literal: true

# This class is responsible for broadcasting updated exchange rate
class ExchangeRateChannel < ApplicationCable::Channel
  CHANNEL_NAME = 'usd_to_rub_rate'

  def subscribed
    stream_from CHANNEL_NAME
  end

  def unsubscribed
    # Any cleanup needed when channel is unsubscribed
  end
end
