import consumer from "./consumer"

function change_usd_to_rub_exchange_rate(new_value) {
  document.getElementById('usd_to_rub_value').textContent = new_value;
}

export { change_usd_to_rub_exchange_rate };

consumer.subscriptions.create("ExchangeRateChannel", {
  connected() {
    // Called when the subscription is ready for use on the server
  },

  disconnected() {
    // Called when the subscription has been terminated by the server
  },

  received(data) {
    change_usd_to_rub_exchange_rate(data);
    // Called when there's incoming data on the websocket for this channel
  }
});
