# frozen_string_literal: true

# This class is responsible for provide API to cbr.ru XML service
class CbrAdapter
  SERVICE_DOMAIN = 'www.cbr.ru'
  SERVICE_URL = "http://#{SERVICE_DOMAIN}/scripts/XML_daily.asp"

  def self.one_usd_in_rub
    xml_data = Net::HTTP.get_response(URI.parse(SERVICE_URL)).body
    xml = Nokogiri::XML(xml_data)

    string_value = xml.xpath('//Valute[@ID="R01235"]').xpath('Value').text

    Float(string_value.tr(',', '.'))
  end
end
