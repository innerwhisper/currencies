# frozen_string_literal: true

REDIS_CONN =
  if Rails.env.test?
    MockRedis.new
  else
    config =
      YAML.load(
        ERB.new(
          File.read(
            Rails.root.join('config/redis.yml')
          )
        ).result
      ).symbolize_keys[Rails.env.to_sym].symbolize_keys

    Redis.new(config)
  end
